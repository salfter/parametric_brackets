use <parametric_bracket.scad>

// settings

nominal_extrusion_width=20; 
screw_diameter=5;
gap=0.4; // adjust for a snug fit
thickness=2.8; 

// other constants needed later

tw=(nominal_extrusion_width+gap+2*thickness); // total width
iw=(nominal_extrusion_width+gap+thickness)/2; // dimension from center of cube to center of hole
sleeve_gap=gap+.2; // looser fit for alignment sleeves


	t();
	translate([-iw,0,tw])
    rotate([0,90,0])
        panel();
	translate([0,iw,tw])
    rotate([90,0,0])
        panel();
	translate([0,0,-tw])
	rotate([90,0,0])
		tube();
	translate([tw,0,0])
	rotate([0,0,90])
		tube();
	translate([tw,0,-tw])
	rotate([180,0,0])
		brace();


