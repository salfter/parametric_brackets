Parametric Extrusion Brackets
=============================

The basic parameters of the design are derived from
https://www.thingiverse.com/thing:1758722, but this is a modular
reimplementation in OpenSCAD.  Three different configurations are provided
out of the box; others are easy to implement with the provided modules.

The pre-rendered STLs are for 2020 extrusion and M5 screws.  You can
re-render the designs for larger or smaller extrusions and/or larger or
smaller hardware.  The wall thickness and tolerance are also quickly
adjusted.

If you use these brackets to build a Hypercube 300, you'll want to use the
alignment sleeves and clip provided here, as the usual alignment tools
([https://www.thingiverse.com/thing:2796070](https://www.thingiverse.com/thing:2796070)) won't fit.

Update (24 Jul 18): added alignment sleeves for Hypercube 300 frame
assembly.  Use the long one to position a T-bracket at the appropriate
distance from the top, cap the top with a corner, and use the short one to
position a double-T bracket at the appropriate distance from the bottom. 
Their length is adjusted according to the parameters set for the brackets.

Update (25 Jul 18): the sleeves were a half-width too long, so they've been
fixed.  Also, they're labeled as to which should be used where, as I got
this mixed up myself when I went to use them.  :)

Update (26 Jul 18): I'd left the SCAD file set to 15-mm extrusion instead of
20, and the alignment sleeves uploaded from it were also for 15-mm
extrusion.  Derp!

Update (27 Jul 18): added a Hypercube 300 alignment clip to line up the
forward Z-axis rod clips.

Update (5 Aug 18): added modified corner and T brackets with inside corners
cut out for more clearance for the CoreXY section of the Hypercube 300. 
Print four of the hc300_top_corners for the top of the frame, and replace
two of the 2020_t_brackets at the front of the frame with
hc300_front_left_ts (mirror one of them for the right front).

Update (12 Aug 18): revised alignment sleeve dimensions in accordance with
the dimensions given in [this Facebook post](https://www.facebook.com/groups/1871049783171141/permalink/2197825837160199/?comment_id=2201728633436586&comment_tracking=%7B%22tn%22%3A%22R%22%7D).

(Also posted to [Thingiverse](https://www.thingiverse.com/thing:3012597).)
