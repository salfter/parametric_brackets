use <parametric_bracket.scad>

// settings

nominal_extrusion_width=20; 
screw_diameter=5;
gap=0.4; // adjust for a snug fit
thickness=2.8; 
integrate_y_rod_mount=1; // include Y-axis rod mounts?
is_gt2=1; // using GT2 pulleys? was going to integrate idlers
y_rod_diam=8; // rod diameter
y_rod_gap=33.5; // gap between top of Y-axis rod and bottom of top extrusion

// other constants needed later

tw=(nominal_extrusion_width+gap+2*thickness); // total width
iw=(nominal_extrusion_width+gap+thickness)/2; // dimension from center of cube to center of hole
sleeve_gap=gap+.2; // looser fit for alignment sleeves


difference()
{
	union()
	{
		corner();
		translate([-iw,tw,0])
		rotate([0,90,0])
			panel();
		translate([0,tw,-iw])
			panel();
		if (integrate_y_rod_mount==1)
		{
			translate([0,tw,iw])
				panel();

			// if (is_gt2==1)
			// {
			// 	translate([8.5+tw/2-thickness,nominal_extrusion_width/2+y_rod_gap+y_rod_diam/2,4.5])
			// 	rotate([0,90,0])
			// 		import("../corexy/GT2 pulley XY idler/xyidler_toothed_pulleys_20mm.stl");
			// }
			// else
			// {
			// 	// FIXME: position
			// 	import("../corexy/original/HC_XY_Idler_Make_2_.stl");
			// }

			difference()
			{
				union()
				{
					translate([-(y_rod_diam+8)/2,(nominal_extrusion_width+gap)/2,(nominal_extrusion_width+gap)/2])
						cube([y_rod_diam+8,y_rod_gap+y_rod_diam/2,7.5]);
					translate([0,nominal_extrusion_width/2+y_rod_gap+y_rod_diam/2,tw/2-thickness])
						cylinder(d=y_rod_diam+8, h=7.5, $fn=30);
				}
				translate([0,nominal_extrusion_width/2+y_rod_gap+y_rod_diam/2,tw/2-thickness])
					cylinder(d=y_rod_diam+gap, h=7.5, $fn=30);
			}
		}
		translate([tw,0,0])
		rotate([0,0,90])
			tube();
		translate([0,0,tw])
		rotate([90,0,0])
			tube();
		translate([tw,0,tw])
			brace();
	}

	translate([iw,tw,0]) // clearance for idler mounts
	rotate([0,90,0])
		panel();
	translate([iw+1,tw,0])
	rotate([0,90,0])
		panel();
}